// Copyright (c) 2020 Omkar Prabhu
#include <torch/torch.h>
#include <utility>

struct ConvLayer : torch::nn::Module {
    torch::nn::Conv2d conv{nullptr};

    ConvLayer(int64_t in_channels = 1,
            int64_t out_channels = 256,
            int64_t kernel_size = 9);

    torch::Tensor forward(torch::Tensor x);
};

struct PrimaryCaps : torch::nn::Module {
    torch::nn::ModuleList capsules{nullptr};

    PrimaryCaps(int64_t num_capsules = 8,
            int64_t in_channels = 256,
            int64_t out_channels = 32,
            int64_t kernel_size = 9);

    torch::Tensor forward(torch::Tensor x);
    torch::Tensor squash(torch::Tensor input_tensor);
};

struct DigitCaps : torch::nn::Module {
    int64_t in_channels, num_routes, num_capsules;
    torch::Tensor W;

    DigitCaps(int64_t num_capsules = 10,
            int64_t num_routes = 32 * 6 * 6,
            int64_t in_channels = 8,
            int64_t out_channels = 16);

    torch::Tensor forward(torch::Tensor x);
    torch::Tensor squash(torch::Tensor input_tensor);
};

struct DecoderOutput {
    torch::Tensor reconstructions;
    torch::Tensor masked;
};

struct Decoder : torch::nn::Module {
    torch::nn::Sequential reconstruction_layers;

    Decoder();

    DecoderOutput forward(torch::Tensor x, torch::Tensor data);
};

struct CapsNetOutput {
    torch::Tensor output;
    torch::Tensor reconstructions;
    torch::Tensor masked;
};

struct CapsNet : torch::nn::Module {
    ConvLayer conv_layer;
    PrimaryCaps primary_capsules;
    DigitCaps digit_capsules;
    Decoder decoder;
    torch::nn::MSELoss mse_loss;

    CapsNet();

    CapsNetOutput forward(torch::Tensor x);
    torch::Tensor loss(torch::Tensor data, torch::Tensor x, torch::Tensor target, torch::Tensor reconstructions);
    torch::Tensor margin_loss(torch::Tensor x, torch::Tensor labels);
    torch::Tensor reconstruction_loss(torch::Tensor data, torch::Tensor reconstructions);
};
