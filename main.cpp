// Copyright (c) 2020 Omkar Prabhu
#include <capsnet.h>
#include <torch/torch.h>
#include <iostream>
#include <tuple>

ConvLayer::ConvLayer(int64_t in_channels, int64_t out_channels, int64_t kernel_size)  {
    conv = torch::nn::Conv2d(torch::nn::Conv2dOptions(in_channels, out_channels, kernel_size));
    register_module("conv", conv);
}

torch::Tensor ConvLayer::forward(torch::Tensor x) {
    return torch::relu(conv->forward(x));
}

PrimaryCaps::PrimaryCaps(int64_t num_capsules,
        int64_t in_channels, int64_t out_channels, int64_t kernel_size) {
    for (int64_t i = 0; i < num_capsules; i++) {
        capsules->push_back(torch::nn::Conv2d(
                torch::nn::Conv2dOptions(in_channels, out_channels, kernel_size).stride(2)));
    }
}

torch::Tensor PrimaryCaps::forward(torch::Tensor x) {
    std::vector<torch::Tensor> us;
    for (size_t i = 0; i < capsules->size(); i++) {
        auto module = capsules[i]->as<torch::nn::Conv2d>();
        us.push_back(module->forward(x));
    }
    auto u = torch::stack(us, 1);
    u = u.view({x.size(0), 32 * 6 * 6, -1});
    return squash(u);
}

torch::Tensor PrimaryCaps::squash(torch::Tensor input_tensor) {
    auto squared_norm = (input_tensor * input_tensor).sum(-1, true);
    return (squared_norm * input_tensor) / ((1.0 + squared_norm) * torch::sqrt(squared_norm));
}

DigitCaps::DigitCaps(int64_t num_capsules,
        int64_t num_routes, int64_t in_channels, int64_t out_channels) {
    DigitCaps::in_channels = in_channels;
    DigitCaps::num_routes = num_routes;
    DigitCaps::num_capsules = num_capsules;
    DigitCaps::W = register_parameter("W", torch::randn({1, num_routes, num_capsules, out_channels, in_channels}));
}

torch::Tensor DigitCaps::forward(torch::Tensor x) {
    auto batch_size = x.size(0);
    std::vector<torch::Tensor> xlist;
    for (int64_t i = 0; i < num_capsules; i++) {
        xlist.push_back(x);
    }
    x = torch::stack(xlist, 2).unsqueeze(4);

    std::vector<torch::Tensor> wlist;
    for (int64_t i = 0; i < batch_size; i++) {
        wlist.push_back(W);
    }
    W = torch::cat(wlist, 0);
    auto u_hat = torch::matmul(W, x);

    auto b_ij = torch::zeros({1, num_routes, num_capsules, 1});

    int64_t num_iterations = 3;
    torch::Tensor v_j;
    for (int64_t i = 0; i < num_iterations; i++) {
        auto c_ij = torch::softmax(b_ij, 1);
        std::vector<torch::Tensor> c_ijlist;
        for (int64_t i = 0; i < batch_size; i++) {
            c_ijlist.push_back(c_ij);
        }
        c_ij = torch::cat(c_ijlist, 0).unsqueeze(4);

        auto s_j = (c_ij * u_hat).sum(1, true);
        v_j = squash(s_j);

        if (i < (num_iterations - 1)) {
            std::vector<torch::Tensor> v_jlist;
            for (int64_t i = 0; i < num_routes; i++) {
                v_jlist.push_back(v_j);
            }
            auto a_ij = torch::matmul(u_hat.transpose(3, 4), torch::cat(v_jlist));
            b_ij = b_ij + a_ij.squeeze(4).mean(0, true);
        }
    }

    return v_j.squeeze(1);
}

torch::Tensor DigitCaps::squash(torch::Tensor input_tensor) {
    auto squared_norm = (input_tensor * input_tensor).sum(-1, true);
    return (squared_norm * input_tensor) / ((1.0 + squared_norm) * torch::sqrt(squared_norm));
}

Decoder::Decoder() {
    reconstruction_layers = torch::nn::Sequential(
        torch::nn::Linear(16 * 10, 512),
        torch::nn::ReLU(true),
        torch::nn::Linear(512, 1024),
        torch::nn::ReLU(true),
        torch::nn::Linear(1024, 784),
        torch::nn::Sigmoid());
}

DecoderOutput Decoder::forward(torch::Tensor x, torch::Tensor data) {
    auto classes = torch::sqrt((x * x).sum(2));
    classes = torch::softmax(classes, 1);

    std::tuple<torch::Tensor, torch::Tensor> maxtuple = classes.max(1);

    auto masked = torch::eye(10);
    masked = masked.index_select(0, std::get<1>(maxtuple));

    auto mulans = x * masked.index({at::indexing::Slice(), at::indexing::Slice(),
                                    at::indexing::None, at::indexing::None});
    auto reconstructions = reconstruction_layers->forward((mulans.view({x.size(0), -1})));
    reconstructions = reconstructions.view({-1, 1, 28, 28});
    return {reconstructions, masked};
}

CapsNet::CapsNet() {
    conv_layer = ConvLayer();
    primary_capsules = PrimaryCaps();
    digit_capsules = DigitCaps();
    decoder = Decoder();
    mse_loss = torch::nn::MSELoss();
}

CapsNetOutput CapsNet::forward(torch::Tensor data) {
    auto output = digit_capsules.forward(primary_capsules.forward(conv_layer.forward(data)));
    DecoderOutput decoder_output = decoder.forward(output, data);
    return {output, decoder_output.reconstructions, decoder_output.masked};
}

torch::Tensor CapsNet::loss(torch::Tensor data, torch::Tensor x,
        torch::Tensor target, torch::Tensor reconstructions) {
    return margin_loss(x, target) + reconstruction_loss(data, reconstructions);
}

torch::Tensor CapsNet::margin_loss(torch::Tensor x, torch::Tensor labels) {
    auto batch_size = x.size(0);

    auto v_c = torch::sqrt((x * x).sum(2, true));

    auto left = torch::relu(0.9 - v_c).view({batch_size, -1});
    auto right = torch::relu(v_c - 0.1).view({batch_size, -1});

    auto loss = labels * left + 0.5 * (1.0 - labels) * right;
    loss = loss.sum(1).mean();

    return loss;
}

torch::Tensor CapsNet::reconstruction_loss(torch::Tensor data, torch::Tensor reconstructions) {
    auto loss = mse_loss(reconstructions.view({reconstructions.size(0), -1}),
            data.view({reconstructions.size(0), -1}));
    return loss * 0.0005;
}

int main() {
    // Initialize Device for CUDA (if any)
    auto cuda_available = torch::cuda::is_available();
    torch::Device device(cuda_available ? torch::kCUDA : torch::kCPU);
    std::cout << (cuda_available ? "CUDA Enabled. Training on GPU" :
                  "Training on CPU") << std::endl;

    // Hyper parameters
    const int64_t batch_size = 100;
    const size_t num_epochs = 30;

    // Loading the dataset
    const std::string MNIST_DATA_ROOT = "../data/";
    auto train_dataset = torch::data::datasets::MNIST(MNIST_DATA_ROOT)
            .map(torch::data::transforms::Normalize<>(0.1307, 0.3081))
            .map(torch::data::transforms::Stack<>());
    auto test_dataset = torch::data::datasets::MNIST(MNIST_DATA_ROOT,
            torch::data::datasets::MNIST::Mode::kTest)
            .map(torch::data::transforms::Normalize<>(0.1307, 0.3081))
            .map(torch::data::transforms::Stack<>());
    auto train_loader = torch::data::make_data_loader<torch::data::samplers::RandomSampler>(
            std::move(train_dataset), batch_size);
    auto test_loader = torch::data::make_data_loader<torch::data::samplers::SequentialSampler>(
            std::move(test_dataset), batch_size);

    // Initializing the model
    CapsNet capsule_net;

    // Criterion and Optimizer
    torch::optim::Adam optimizer(capsule_net.parameters());

    for (size_t i = 0; i < num_epochs; i++) {
        // Training
        capsule_net.train();
        auto train_loss = torch::tensor(0.0);

        auto batch_id = 0;
        for (auto &batch : *train_loader) {
            auto data = batch.data.to(device);
            auto target = batch.target.to(device);

            target = torch::eye(10).index_select(0, target);

            optimizer.zero_grad();
            CapsNetOutput capsNetOutput = capsule_net.forward(data);
            auto loss = capsule_net.loss(data, capsNetOutput.output,
                    target, capsNetOutput.reconstructions);
            loss.backward();
            optimizer.step();

            train_loss += loss.data()[0];

            if (batch_id % 100 == 0) {
                auto train_acc = sum(torch::argmax(target.data(), 1)
                        == torch::argmax(capsNetOutput.masked.data(), 1)) / float(batch_size);
                std::cout << "Train Accuracy: " << train_acc.data() << std::endl;
            }
            batch_id++;
        }
    }
}
