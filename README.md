# Dynamic Routing Between Capsules
PyTorch C++ Implementation of "Dynamic Routing Between Capsules" [NIPS 2017]

![Paper](http://img.shields.io/badge/paper-arxiv.1710.09829-B31B1B.svg)
![Conference](http://img.shields.io/badge/NIPS-2017-4b44ce.svg)
![LibTorch](https://img.shields.io/badge/libtorch-1.4-%23ee4c2c)
![Dataset](https://img.shields.io/badge/dataset-mnist-blueviolet)
![C++](https://img.shields.io/badge/c++-14-informational)

## About
This is an unofficial repository accompanying the NIPS 2017 paper 
_[Dynamic Routing Between Capsules](https://arxiv.org/pdf/1710.09829.pdf)_. 
This repository contains the instructions to download the dataset and codebase.

## Requirements
1. [C++](http://www.cplusplus.com/doc/tutorial/introduction/)
2. [CMake](https://cmake.org/download/)
3. [LibTorch v1.4.0](https://pytorch.org/cppdocs/installing.html)
4. [MNIST Dataset](http://yann.lecun.com/exdb/mnist/index.html)

## Running
Follow the instructions given below to train and test using PyTorch C++

### Training & Testing

```shell
mkdir build
cd build
cmake -DCMAKE_PREFIX_PATH=/path/to/libtorch ..
make
./capsnet
```

## Acknowledgements
   
We thank the authors for sharing their work via [paper](https://arxiv.org/pdf/1710.09829.pdf).

## Citation
   
```
@inproceedings{10.5555/3294996.3295142,
    author = {Sabour, Sara and Frosst, Nicholas and Hinton, Geoffrey E.},
    title = {Dynamic Routing between Capsules},
    year = {2017},
    isbn = {9781510860964},
    publisher = {Curran Associates Inc.},
    address = {Red Hook, NY, USA},
    booktitle = {Proceedings of the 31st International Conference on Neural Information Processing Systems},
    pages = {3859–3869},
    numpages = {11},
    location = {Long Beach, California, USA},
    series = {NIPS’17}
}
```